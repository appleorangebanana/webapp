const Minio = require('minio');
const config = require('../config');
const crypto = require('crypto');
const fs = require("fs");
const mime = require("mime-types");
const log = require('../lib/logger');

const minioClient = new Minio.Client({
    endPoint: config.storageConfig.endpoint,
    port: config.storageConfig.port,
    useSSL: config.storageConfig.useSSL,
    accessKey: config.storageConfig.accessKey,
    secretKey: config.storageConfig.secretKey
});

minioClient.pubPolicyVar = {
    Version:"2012-10-17",
    Statement:[
        {
            Sid: "PublicRead",
            Effect: "Allow",
            Principal: "*",
            Action: ["s3:GetObject","s3:GetObjectVersion"],
            Resource: [`arn:aws:s3:::${config.storageConfig.bucketNamePub}/*`]
        }
    ]
};

minioClient.uploadPublic = async (file, path) => {
    log.info(`Uploading ${file.name} of type ${file.type} of size ${file.size} at ${file.path}`);
    let timestamp = new Date().getTime();
    let buffer = await fs.readFileSync(file.path);
    let hash = await crypto.createHash('md5').update(fs.readFileSync(file.path)).digest('hex');
    log.info(`Generated hash of ${hash} at ${timestamp}`);
    let generatedName = `${hash}-${timestamp}.${mime.extension(file.type)}`;
    let generatedPath = `${path}${generatedName}`;
    log.info(`Storing at ${generatedPath}`);
    let metadata = {'Content-Type': file.type};
    await minioClient.putObject(config.storageConfig.bucketNamePub, generatedPath, buffer, file.size, metadata);
    // Get URL
    let header = "http";
    if (config.storageConfig.useSSL) header += "s";
    return {location: `${header}://${config.storageConfig.endpoint}:${config.storageConfig.port}/${config.storageConfig.bucketNamePub}/${generatedPath}`};
}

module.exports = minioClient;