const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const appUtil = require('../lib/util');
const log = require('../lib/logger');

const db = require('../lib/db');
const cache = require('../lib/redis');
const { data } = require('jquery');

const renderConf = {appName: 'Survey Management - AASP', route: 'Admin'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });

// Access list feedback page
router.get('/listfeedbacks', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewfeedbacks');
    res.render('survey/listfeedbacks', {...renderConf, title: 'Feedbacks'});
});

// Submit Survey
router.post('/submit', async (req, res) => {
    try {
        appUtil.submitFeedback(req.body.userid, req.body.quizid, req.body.rating, req.body.feedback);
        return
    } catch (err) {
        log.error(err);
    }
});

// List all feedbacks
router.get('/feedbackdata', async (req, res) => {
    let success = '', failure = '';
    let [feedbacks] = await db.query('SELECT feedbacks.id, users.fullname, users.email, quiz.name as quiz, feedbacks.rating, feedbacks.feedback FROM feedbacks INNER JOIN users ON feedbacks.userid = users.id INNER JOIN quiz ON feedbacks.quizid = quiz.id');
    let data = [];
    for (let feedback of feedbacks) {
        log.debug(feedback);
        data.push({id: feedback.id, username: feedback.fullname, email: feedback.email, quizname: feedback.quiz, rating: feedback.rating, feedback: feedback.feedback});
    }
    res.json({data: data});
});



module.exports = router;
