require('dotenv').config();

let dbConfig = {
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    port: process.env.MYSQL_PORT || 3306,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE
};

let redisConfig = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT || 6379,
    ttl: process.env.REDIS_TTL || 86400, // 24 hours session expiry, change in env var
    secret: process.env.REDIS_SECRET,
    password: process.env.REDIS_PASSWORD,
    montiorMode: process.env.REDIS_MONITOR || false
}

let storeSSL = (process.env.STORAGE_SSL) ? process.env.STORAGE_SSL.toLowerCase() === 'true' : true;

let storageConfig = {
    endpoint: process.env.STORAGE_ENDPOINT,
    port: parseInt(process.env.STORAGE_PORT) || 9000,
    useSSL: storeSSL,
    accessKey: process.env.STORAGE_ACCESS_KEY,
    secretKey: process.env.STORAGE_SECRET_KEY,
    bucketRegion: process.env.STORAGE_REGION || "ap-southeast-1",
    bucketName: process.env.STORAGE_BUCKET_SECRET,
    bucketNamePub: process.env.STORAGE_BUCKET_PUBLIC
}

// Default 10 requests every 5 minutes, block for a minute
let rateLimitConfig = {
    request: process.env.RATELIMIT_REQUEST || 10,
    period: process.env.RATELIMIT_PERIOD || 300,
    block: process.env.RATELIMIT_BLOCK || 60
}

let miscConfigs = {
    tinymce: process.env.TINYMCE_API || 'no-api-key',
    otpExpiry: parseInt(process.env.OTP_EXPIRY) || 120,
    otpLength: parseInt(process.env.OTP_LENGTH) || 6,
    submissionQueue: process.env.QUEUE_SUBMISSION || 'submissionqueue',
    codeQueue: process.env.QUEUE_CODE || 'codequeue',
    mathQueue: process.env.QUEUE_MATH || 'mathqueue',
    structuredQueue: process.env.QUEUE_STRUCTURED || 'structuredqueue',
    mcqQueue: process.env.QUEUE_MCQ || 'mcqqueue',
    trustProxy: (process.env.TRUST_PROXY === 'true') || false
};

module.exports = { dbConfig, redisConfig, storageConfig, rateLimitConfig, miscConfigs };